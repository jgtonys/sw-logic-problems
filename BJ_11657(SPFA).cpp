#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>

#define inf 1000000000

using namespace std;
int N,M;
int A,B,C;


int main() {
    cin >> N >> M;
    vector< pair<int,int> > adj[M+1];
    int len[N+1];
    int check[N+1];
    for(int i=0;i<M;i++) {
        cin >> A >> B >> C;
        adj[A-1].push_back( make_pair(B-1,C));
    }

    fill(len,len+N,inf);
    fill(check,check+N,0);

    queue<int> q;
    q.push(0);
    check[0] = 1;
    len[0] = 0;
    int ct = 0;
    while(!q.empty()) {
        if(ct >= N) {
            cout << -1 << endl;
            return 0;
        }
        int qsize = q.size();
        while(qsize--) {
            int u = q.front();
            q.pop();
            check[u] = 0;

            for(int i=0;i<adj[u].size();i++) {
                int v = adj[u][i].first;
                int cost = adj[u][i].second;
                if(len[v] > len[u] + cost) {
                    len[v] = len[u] + cost;
                    if(check[v] == 0) {
                        q.push(v);
                        check[v] = 1;
                    }
                }
            }
        }
        ct++;
    }
    
    for (int i = 1; i < N; i++) {
        if(len[i]==inf) cout << -1 << endl;
        else cout << len[i] << endl;
	}

    return 0;
}